// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileUtilities.generated.h"

/**
 * 
 */

class FJsonObject;

USTRUCT()
struct FJSONData
{
	//Estructura con formato de los datos a guardar en el JSON

	GENERATED_BODY()

	UPROPERTY()
	FString Name;

	UPROPERTY()
	TMap<FString, bool> Properties;
};


USTRUCT()
struct FJSONStruct
{
	//Estructura utilizada para guardar multiples JSON
	
	GENERATED_BODY()

	UPROPERTY()
	TArray<FJSONData> Data;
};


UCLASS()
class UTILITIESEXERCISES_API UFileUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	static void WriteFile(const FString& InFilePath, const FString& InContent);
	
	static void WriteJsonFile(const FString& InJSONPath, const TSharedPtr<FJsonObject>& InJsonObject);

public:
	static void WriteStructToJson(const FString& InJSONPath, const FJSONStruct& InStruct);
};
