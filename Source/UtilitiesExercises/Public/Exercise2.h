// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Exercise2.generated.h"

/**
 * 
 */
UCLASS()
class UTILITIESEXERCISES_API UExercise2 : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	static const int16 MAXTRIS = 10000;
	static const int8 MAXMATERALS = 0;
	static const int16 MAXSPACE = 6000;
	
#if WITH_EDITOR
	
	UFUNCTION(BlueprintCallable)
	static void ListActors();
	
#endif
};
