// Copyright Epic Games, Inc. All Rights Reserved.

#include "UtilitiesExercises.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UtilitiesExercises, "UtilitiesExercises" );
