// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UtilitiesExercisesGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UTILITIESEXERCISES_API AUtilitiesExercisesGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
