// Fill out your copyright notice in the Description page of Project Settings.


#include "UtilitiesExercises/Public/FileUtilities.h"
#include "JsonObjectConverter.h"
#include "Misc/FileHelper.h"
#include "Serialization/JsonSerializer.h"

void UFileUtilities::WriteFile(const FString& InFilePath, const FString& InContent)
{
	if(!FFileHelper::SaveStringToFile(InContent, *InFilePath))
	{
		UE_LOG(LogTemp, Error, TEXT("ERROR al salvar el archivo: %s"), *InFilePath);
	}
	UE_LOG(LogTemp, Display, TEXT("ARCHIVO %s guardado con exito"), *InFilePath);
}

void UFileUtilities::WriteJsonFile(const FString& InJSONPath, const TSharedPtr<FJsonObject>& InJsonObject)
{
	FString JSONString;
	if(!FJsonSerializer::Serialize(InJsonObject.ToSharedRef(), TJsonWriterFactory<>::Create(&JSONString, 0)))
	{
		UE_LOG(LogTemp, Error, TEXT("ERROR al salvar el archivo: %s"), *InJSONPath);
	}
	WriteFile(InJSONPath, JSONString);
}

void UFileUtilities::WriteStructToJson(const FString& InJSONPath, const FJSONStruct& InStruct)
{
	const TSharedPtr<FJsonObject> JsonObject = FJsonObjectConverter::UStructToJsonObject(InStruct);
	WriteJsonFile(InJSONPath, JsonObject);
}
