// Fill out your copyright notice in the Description page of Project Settings.


#include "Exercise2.h"

#include "AssetToolsModule.h"
#include "FileUtilities.h"
#include "HAL/FileManagerGeneric.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Subsystems/EditorActorSubsystem.h"


void UExercise2::ListActors()
{
	//Arreglo donde se guardaran los Assets a exportar
	TArray<FString> AssetsToExport;

	//Estrucutura utilizada para generar el archivo JSON con detalle
	FJSONStruct JSONStruct;
	
	
	//Obtengo todos los Componentes del nivel
	UEditorActorSubsystem* Eas = GEditor->GetEditorSubsystem<UEditorActorSubsystem>();
	if (!Eas)
	{
		UE_LOG(LogTemp, Error, TEXT("Editor Actor Subsystem not found!"));
	}
	TArray<UActorComponent*> Components = Eas->GetAllLevelActorsComponents();

	//Itero por los componentes quedandome solo con aquellos que pueden ser casteados a StaticMeshComponent
	for (const auto ActorComponent : Components)
	{
		if (const UStaticMeshComponent* SMComponent = Cast<UStaticMeshComponent>(ActorComponent))
		{
			if(UStaticMesh* SM = SMComponent->GetStaticMesh();
				SM->GetNumTriangles(0) > MAXTRIS
				|| SM->GetStaticMaterials().Max() > MAXMATERALS)
			{
				//No se como hacer lo del espacio absoluto, tengo que repasar geometria 
				UE_LOG(LogTemp, Error, TEXT("SIZE %s"), *SM->GetBoundingBox().GetSize().ToString());
				UE_LOG(LogTemp, Error, TEXT("CENTER %s"), *SM->GetBoundingBox().GetCenter().ToString());
				UE_LOG(LogTemp, Error, TEXT("EXTENT %s"), *SM->GetBoundingBox().GetExtent().ToString());

				AssetsToExport.Add(SM->GetPathName());

				//Generacion de structura con datos del archivo JSON
				FJSONData JSONData;
				JSONData.Name = SM->GetFullName(); 
				JSONData.Properties.Add("bHasManyTri", SM->GetNumTriangles(0) > MAXTRIS);
				JSONData.Properties.Add("bHasManyMats", SM->GetStaticMaterials().Max() > MAXMATERALS);
				JSONData.Properties.Add("bIsMassive", true);
				JSONStruct.Data.Add(JSONData);
			}
		}
	}
	if(!AssetsToExport.IsEmpty())
	{
		//Obtengo el directorio del proyecto y elimino el folder Saved/Rework
		const FString FolderPath = UKismetSystemLibrary::GetProjectDirectory() + "Saved/Rework";
		FFileManagerGeneric FManager = FFileManagerGeneric();
		FManager.DeleteDirectory(*FolderPath, 0, 1);
		
		//Exporto los Assets y escribo el archivo JSON con el detalle
		const FAssetToolsModule& AssetToolsModule = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools");
		AssetToolsModule.Get().ExportAssets(AssetsToExport, *FolderPath);
		UFileUtilities::WriteStructToJson(FolderPath + "/Exercise2.json", JSONStruct);	
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("NOTHING to export"));
	}
}
